package com.app.trptuition.security.util;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;

import static com.app.trptuition.security.util.ApplicationUserAuthorities.*;

public enum ApplicationUserRole {
    ADMIN(Set.of(READ_EXAM, WRITE_EXAM, READ_QUESTION, WRITE_QUESTION, READ_SUBJECT, WRITE_SUBJECT, READ_RESULT,WRITE_RESULT)),
    STUDENT(Set.of(READ_EXAM, WRITE_EXAM, READ_QUESTION, READ_SUBJECT, READ_RESULT)),
    STAFF(Set.of(READ_EXAM, WRITE_EXAM, READ_QUESTION, WRITE_QUESTION, READ_SUBJECT, WRITE_SUBJECT, READ_RESULT,WRITE_RESULT)),
    PARENT(Set.of(READ_EXAM, READ_QUESTION, READ_SUBJECT, READ_RESULT ));

    private final Set<ApplicationUserAuthorities> authorities;

    ApplicationUserRole(Set<ApplicationUserAuthorities> authorities) {
        this.authorities = authorities;
    }

    public Set<ApplicationUserAuthorities> getAuthorities() {
        return authorities;
    }

    public Set<SimpleGrantedAuthority> getGrantedAuthority(){
        Set<SimpleGrantedAuthority> copyOfAuthorities = getAuthorities()
                .stream()
                .map(authority -> new SimpleGrantedAuthority(authority.getAuthorities()))
                .collect(Collectors.toSet());
        copyOfAuthorities.add(new SimpleGrantedAuthority("ROLE_"+ this.name()));
        return copyOfAuthorities;
    }
}
