package com.app.trptuition.security.util;

import com.app.trptuition.entity.enums.Role;
import com.app.trptuition.security.model.ApplicationUser;
import com.app.trptuition.security.service.AuthenticationService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;

import org.springframework.stereotype.Component;


import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Component
public class JwtUtil {

    private static Logger LOGGER = LoggerFactory.getLogger(AuthenticationService.class);
    private final String SECRET_KEY = "secret_secret_secret";
    private final String CLAIM_NAME = "Authorities";
    private final String ROLE = "role";

    public String generateToken(Authentication authentication) {
        ApplicationUser applicationUser = (ApplicationUser) authentication.getPrincipal();

        return Jwts.builder()
                .setSubject(applicationUser.getUsername())
                .claim(CLAIM_NAME, authentication.getAuthorities())
                .claim(ROLE, applicationUser.getRole().name())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .signWith(SignatureAlgorithm.HS256, getKey())
                .setExpiration(getTokenAge())
                .compact();
    }

    public Date getTokenAge(){
        return new Date(System.currentTimeMillis() + 1000 * 60 * 60 * 10);
    }

    public String getKey() {
        return SECRET_KEY;
    }

    public boolean validateToken(String jwt) {
        Jwts.parser().setSigningKey(getKey()).parseClaimsJws(jwt);
        return true;
    }

    public String getUsernameFromJwt(String jwt) {
        Claims claims = Jwts.parser()
                .setSigningKey(getKey())
                .parseClaimsJws(jwt)
                .getBody();
        return claims.getSubject();
    }

    public String getUserRoleFromJwt(String jwt){
        Claims claims = Jwts.parser()
                .setSigningKey(getKey())
                .parseClaimsJws(jwt)
                .getBody();
        return claims.get(ROLE, String.class);
    }

    public String generateTokenWithUsername(String username) {
        return Jwts.builder()
                .setSubject(username)
                .setIssuedAt(Date.from(Instant.now()))
                .signWith(SignatureAlgorithm.HS256, getKey())
                .setExpiration(getTokenAge())
                .compact();
    }

}
