package com.app.trptuition.security.util;

public enum ApplicationUserAuthorities {
    READ_EXAM("read:exam"),
    WRITE_EXAM("write:exam"),
    READ_QUESTION("read:question"),
    WRITE_QUESTION("write:question"),
    READ_SUBJECT("read:subject"),
    WRITE_SUBJECT("write:subject"),
    READ_RESULT("read:result"),
    WRITE_RESULT("write:result");

    private final String authorities;

    ApplicationUserAuthorities(String authorities) {
            this.authorities = authorities;
    }

    public String getAuthorities() {
        return authorities;
    }
}
