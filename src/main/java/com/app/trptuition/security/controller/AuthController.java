package com.app.trptuition.security.controller;

import com.app.trptuition.security.model.AuthenticationRequest;
import com.app.trptuition.security.model.AuthenticationResponse;
import com.app.trptuition.security.service.AuthenticationService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/auth")
public class AuthController {

    private final AuthenticationService authenticationService$;

    @PostMapping("/login") // student, staff, parent
    public ResponseEntity<AuthenticationResponse> logIn(@RequestBody AuthenticationRequest authenticationRequest){
        AuthenticationResponse authenticationResponse = this.authenticationService$.logIn(authenticationRequest);
        return new ResponseEntity<>(authenticationResponse, HttpStatus.OK);
    }

}
