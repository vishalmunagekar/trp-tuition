package com.app.trptuition.security.service;

import com.app.trptuition.controller.TestController;
import com.app.trptuition.entity.sql.User;
import com.app.trptuition.security.model.ApplicationUser;
import com.app.trptuition.security.util.JwtUtil;
import com.app.trptuition.security.util.ApplicationUserRole;
import com.app.trptuition.service.ParentService;
import com.app.trptuition.service.StaffService;
import com.app.trptuition.service.StudentService;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Service
@AllArgsConstructor
public class SSUserDetailsService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    private final StaffService staffService$;
    private final StudentService studentService$;
    private final ParentService parentService$;
    private final JwtUtil jwtUtil;
    private final String HEADER = "Authorization";
    private final String REQUEST_PARAMETER = "userType";

    @Override
    public ApplicationUser loadUserByUsername(String username) {

        String userType = (((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest().getParameter(REQUEST_PARAMETER));
        LOGGER.info("userType : " + userType);

        String token = (((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest().getHeader(HEADER));
        LOGGER.info("Token :" + token);

        if(StringUtils.hasText(token))
            userType = jwtUtil.getUserRoleFromJwt(token.substring(7));

        return fetchUserByUserRole(userType, username);
    }

    private ApplicationUser fetchUserByUserRole(String userType, String username){
        User user = null;
        switch (userType.toUpperCase()) {
            case "STUDENT":
                user = this.studentService$.getStudentByUsername(username);
                LOGGER.info("User logged in as a Student");
                break;
            case "STAFF":
                user = this.staffService$.getStaffByUsername(username);
                LOGGER.info("User logged in as a Staff");
                break;
            case "PARENT":
                user = this.parentService$.getParentByUsername(username);
                LOGGER.info("User logged in as a Parent");
                break;
            default:
                LOGGER.info("Somthing went wrong...");
        }
        return new ApplicationUser(
                user.getUsername(),
                user.getPassword(),
                ApplicationUserRole.valueOf(user.getRole().name()).getGrantedAuthority(),
                user.getRole());
    }
}