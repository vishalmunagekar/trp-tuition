package com.app.trptuition.repository.sql;

import com.app.trptuition.entity.sql.Standard;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StandardRepository extends JpaRepository<Standard, Long> {
    Optional<Standard> getStandardByStandardName(String standardName);
}
