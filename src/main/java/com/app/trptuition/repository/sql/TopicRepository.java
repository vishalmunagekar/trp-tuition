package com.app.trptuition.repository.sql;

import com.app.trptuition.entity.sql.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Long> {
    Topic getTopicByTopicName(String topicName);
}
