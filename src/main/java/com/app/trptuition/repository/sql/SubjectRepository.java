package com.app.trptuition.repository.sql;

import com.app.trptuition.entity.sql.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {
    Subject getSubjectByNameOfSubject(String nameOfSubject);
}
