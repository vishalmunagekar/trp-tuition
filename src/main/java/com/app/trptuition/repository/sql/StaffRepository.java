package com.app.trptuition.repository.sql;

import com.app.trptuition.entity.sql.Staff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StaffRepository extends JpaRepository<Staff, Long> {
    Optional<Staff> getStaffByUsername(String username);
}
