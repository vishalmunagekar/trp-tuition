package com.app.trptuition.repository.sql;

import com.app.trptuition.entity.sql.Batch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BatchRepository extends JpaRepository<Batch, Long> {
    Optional<Batch> getBatchByBatchName(String batchName);
}
