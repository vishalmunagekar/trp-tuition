package com.app.trptuition.repository.sql;

import com.app.trptuition.entity.sql.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    Optional<Student> getStudentByUsername(String username);
}
