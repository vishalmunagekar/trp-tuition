package com.app.trptuition.repository.sql;

import com.app.trptuition.entity.sql.Parent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ParentRepository extends JpaRepository<Parent, Long> {
    Optional<Parent> getParentByUsername(String username);
}
