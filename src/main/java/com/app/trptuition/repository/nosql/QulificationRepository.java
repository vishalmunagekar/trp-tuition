package com.app.trptuition.repository.nosql;

import com.app.trptuition.entity.nosql.Qualification;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QulificationRepository extends MongoRepository<Qualification, String> {
}
