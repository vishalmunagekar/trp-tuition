package com.app.trptuition.repository.nosql;

import com.app.trptuition.entity.nosql.Question;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends MongoRepository<Question, String> {
}
