package com.app.trptuition.repository.nosql;

import com.app.trptuition.entity.nosql.School;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolRepository extends MongoRepository<School, String> {
}
