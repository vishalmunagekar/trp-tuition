package com.app.trptuition.repository.nosql;

import com.app.trptuition.entity.nosql.Option;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OptionRepository extends MongoRepository<Option, String> {
}
