package com.app.trptuition.repository.nosql;

import com.app.trptuition.entity.nosql.Address;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends MongoRepository<Address, String> {
}
