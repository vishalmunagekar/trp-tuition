package com.app.trptuition.repository.nosql;

import com.app.trptuition.entity.nosql.QuestionDetail;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionDetailRepository extends MongoRepository<QuestionDetail, String> {
}
