package com.app.trptuition.service;

import com.app.trptuition.entity.sql.Subject;
import com.app.trptuition.entity.sql.Topic;
import com.app.trptuition.model.SubjectDto;
import com.app.trptuition.model.TpicDto;
import com.app.trptuition.repository.sql.SubjectRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class SubjectService {

    private final SubjectRepository subjectRepository;

    public Subject saveNewSubject(SubjectDto subjectDto){
        Subject subject = Subject.builder().nameOfSubject(subjectDto.getNameOfSubject())
                .standard(subjectDto.getStandard())
                .staff(subjectDto.getStaff())
                .topics(subjectDto.getTopics())
                .build();
        return subjectRepository.save(subject);
    }

    public List<Subject> getAllSubjects(){
        return subjectRepository.findAll();
    }

    public void deleteSubjectById(Long id){
        subjectRepository.deleteById(id);
    }

    public Subject getTopicById(Long id){
        return subjectRepository.getById(id);
    }

    public Subject updateSubject(SubjectDto newSubjectDto){
        Subject oldSubject = subjectRepository.getSubjectByNameOfSubject(newSubjectDto.getNameOfSubject());

        oldSubject.setNameOfSubject(newSubjectDto.getNameOfSubject());
        oldSubject.setStandard(newSubjectDto.getStandard());
        oldSubject.setStaff(newSubjectDto.getStaff());
        oldSubject.setTopics(newSubjectDto.getTopics());

        return subjectRepository.save(oldSubject);
    }
}
