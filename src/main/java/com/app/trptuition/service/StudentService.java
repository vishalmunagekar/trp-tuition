package com.app.trptuition.service;

import com.app.trptuition.entity.sql.Student;
import com.app.trptuition.model.StudentDto;
import com.app.trptuition.repository.sql.StudentRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class StudentService {

    private final StudentRepository studentRepository;

    public Student saveNewStudent(StudentDto studentDto){
        Student student = Student.builder().firstName(studentDto.getFirstName())
                .middleName(studentDto.getMiddleName())
                .lastName(studentDto.getLastName())
                .admissionDate(Instant.now())
                .addressId(studentDto.getAddressId())
                .parentsDetails(studentDto.getParentsDetails())
                .schoolDetailsId(studentDto.getSchoolDetailsId())
                .standard(studentDto.getStandard())
                .build();
        return studentRepository.save(student);
    }

    public List<Student> getAllStudents(){
        return studentRepository.findAll();
    }

    public void deleteStudentById(Long studentId){
        studentRepository.deleteById(studentId);
    }

    public Student getStudentById(Long studentId){
        return studentRepository.getById(studentId);
    }

    public Student updateStudent(StudentDto newStudentDto){
        Optional<Student> optionalUser = studentRepository.getStudentByUsername(newStudentDto.getUsername());
        Student oldStudent = optionalUser.orElseThrow(() ->
                new UsernameNotFoundException("No user found with username :" + newStudentDto.getUsername()));

        oldStudent.setFirstName(newStudentDto.getFirstName());
        oldStudent.setMiddleName(newStudentDto.getMiddleName());
        oldStudent.setLastName(newStudentDto.getLastName());
        //oldStudent.setAddressId(); //we are not going update this date
        oldStudent.setParentsDetails(newStudentDto.getParentsDetails());
        oldStudent.setSchoolDetailsId(newStudentDto.getSchoolDetailsId());
        oldStudent.setAddressId(newStudentDto.getAddressId());
        oldStudent.setStandard(newStudentDto.getStandard());

        return studentRepository.save(oldStudent);
    }

    public Student getStudentByUsername(String username){
        Optional<Student> optionalUser = studentRepository.getStudentByUsername(username);
        Student student = optionalUser.orElseThrow(() ->
                new UsernameNotFoundException("No user found with username :" + username));
        return student;
    }
}
