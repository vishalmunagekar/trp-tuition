package com.app.trptuition.service;


import com.app.trptuition.entity.sql.Topic;
import com.app.trptuition.model.TpicDto;
import com.app.trptuition.repository.sql.TopicRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class TopicService {

    private final TopicRepository topicRepository;

    public Topic saveNewTopic(TpicDto tpicDto){
        Topic topic = Topic.builder().questionsId(tpicDto.getQuestionsId()).subject(tpicDto.getSubject())
                .topicName(tpicDto.getTopicName()).build();
        return topicRepository.save(topic);
    }

    public List<Topic> getAllTopics(){
        return topicRepository.findAll();
    }

    public void deleteTopicById(Long id){
        topicRepository.deleteById(id);
    }

    public Topic getTopicById(Long id){
        return topicRepository.getById(id);
    }

    public Topic updateTopic(Topic newTopic){
        Topic oldTopic = topicRepository.getTopicByTopicName(newTopic.getTopicName());

        oldTopic.setQuestionsId(newTopic.getQuestionsId());
        oldTopic.setSubject(newTopic.getSubject());
        oldTopic.setTopicName(newTopic.getTopicName());

        return topicRepository.save(oldTopic);
    }
}
