package com.app.trptuition.service;

import com.app.trptuition.entity.sql.Staff;
import com.app.trptuition.model.StaffDto;
import com.app.trptuition.repository.sql.StaffRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class StaffService {

    private StaffRepository staffRepository;

    public Long saveNewStaff(StaffDto staffDto) {
        Staff staff = Staff.builder().firstName(staffDto.getFirstName()).middleName(staffDto.getMiddleName())
                .lastName(staffDto.getLastName()).addressId(staffDto.getAddressId()).joiningDate(staffDto.getJoiningDate())
                .qualificationsId(staffDto.getQualificationsId()).standards(staffDto.getStandards())
                .subjects(staffDto.getSubjects()).build();

        return staffRepository.save(staff).getStaffId();
    }

    public List<Staff> getAllStaff(){
        return staffRepository.findAll();
    }

    public void deleteStaffById(Long staffId){
        staffRepository.deleteById(staffId);
    }

    public Staff getStaffById(Long staffId){
        return staffRepository.getById(staffId);
    }

    public Staff updateStaff(StaffDto newStaffDto){
        Optional<Staff> optionalUser = staffRepository.getStaffByUsername(newStaffDto.getUsername());
        Staff oldStaff = optionalUser.orElseThrow(() ->
                new UsernameNotFoundException("No user found with username : " + newStaffDto.getUsername()));

        oldStaff.setFirstName(newStaffDto.getFirstName());
        oldStaff.setMiddleName(newStaffDto.getMiddleName());
        oldStaff.setLastName(newStaffDto.getLastName());
        //oldStaff.setJoiningDate(newStaffDto.getJoiningDate()); //we will not going to chnage this one assigned
        oldStaff.setQualificationsId(newStaffDto.getQualificationsId());
        oldStaff.setStandards(newStaffDto.getStandards());
        oldStaff.setSubjects(newStaffDto.getSubjects());
        oldStaff.setAddressId(newStaffDto.getAddressId());

        return staffRepository.save(oldStaff);
    }

    public Staff getStaffByUsername(String username){
        Optional<Staff> optionalUser = this.staffRepository.getStaffByUsername(username);
        return optionalUser.orElseThrow(() -> new UsernameNotFoundException("No Staff found with username :" + username));
    }
}
