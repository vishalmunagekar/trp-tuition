package com.app.trptuition.service;

import com.app.trptuition.entity.sql.Standard;
import com.app.trptuition.model.StandardDto;
import com.app.trptuition.repository.sql.StandardRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@AllArgsConstructor
public class StandardService {

    private final StandardRepository standardRepository;

    public Standard saveNewStandard(StandardDto standardDto){
        Standard standard = Standard.builder()
                .batch(standardDto.getBatch())
                .staff(standardDto.getStaff())
                .standardName(standardDto.getStandardName())
                .students(standardDto.getStudents())
                .subjects(standardDto.getSubjects())
                .build();
        return standardRepository.save(standard);
    }

    public List<Standard> getAllStandards(){
        return standardRepository.findAll();
    }

    public void deleteStandardById(Long standardId){
        standardRepository.deleteById(standardId);
    }

    public Standard getStandardById(Long standardId){
        return standardRepository.getById(standardId);
    }

    public Standard updateStandard(StandardDto newStandardDto){
        Optional<Standard> optionalStandard = standardRepository.getStandardByStandardName(newStandardDto.getStandardName());
        Standard oldStudent = optionalStandard.orElseThrow(() ->
                new UsernameNotFoundException("No user found with username :" + newStandardDto.getStandardName()));



        return standardRepository.save(oldStudent);
    }

    public Standard getStandardByStandardName(String standardName){
        Optional<Standard> optionalStandard = standardRepository.getStandardByStandardName(standardName);
        return optionalStandard.orElseThrow(() ->
                new UsernameNotFoundException("No Standard found with standardName :" + standardName));

    }
}
