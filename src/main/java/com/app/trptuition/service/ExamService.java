package com.app.trptuition.service;

import com.app.trptuition.entity.enums.PaperSet;
import com.app.trptuition.entity.sql.*;
import com.app.trptuition.model.ExamDto;
import com.app.trptuition.model.ParentDto;
import com.app.trptuition.repository.sql.ExamRepository;
import com.app.trptuition.repository.sql.ParentRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public class ExamService {

    private ExamRepository examRepository;

    public Long saveNewExam(ExamDto examDto) {
        Exam exam = Exam.builder().build();

        return examRepository.save(exam).getExamId();
    }

    public List<Exam> getAllExam(){
        return examRepository.findAll();
    }

    public void deleteExamById(Long parentId){
        examRepository.deleteById(parentId);
    }

    public Exam getExamById(Long examId){
        return examRepository.getById(examId);
    }

    public Exam updateExam(ExamDto newExamDto){
        Exam oldExam = getExamById(newExamDto.getExamId());

        oldExam.setStandard(newExamDto.getStandard());
        oldExam.setSubject(newExamDto.getSubject());
        oldExam.setTopic(newExamDto.getTopic());
        oldExam.setTotalQuestions(newExamDto.getTotalQuestions());
        oldExam.setTotalMarks(newExamDto.getTotalMarks());
        oldExam.setDuration(newExamDto.getDuration());
        oldExam.setExamDate(newExamDto.getExamDate());
        oldExam.setStartTime(newExamDto.getStartTime());
        oldExam.setPaperSetter(newExamDto.getPaperSetter());
        oldExam.setPaperSet(newExamDto.getPaperSet());
        oldExam.setQuestionsId(newExamDto.getQuestionsId());

        return examRepository.save(oldExam);
    }
}
