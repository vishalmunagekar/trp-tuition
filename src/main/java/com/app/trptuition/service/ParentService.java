package com.app.trptuition.service;

import com.app.trptuition.entity.sql.Parent;
import com.app.trptuition.entity.sql.Staff;
import com.app.trptuition.model.ParentDto;
import com.app.trptuition.model.StaffDto;
import com.app.trptuition.repository.sql.ParentRepository;
import com.app.trptuition.repository.sql.StaffRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ParentService {

    private final ParentRepository parentRepository;

    public Long saveNewParent(ParentDto parentDto) {
        Parent parent = Parent.builder().build();

        return parentRepository.save(parent).getParentId();
    }

    public List<Parent> getAllParents(){
        return parentRepository.findAll();
    }

    public void deleteParentById(Long parentId){
        parentRepository.deleteById(parentId);
    }

    public Parent getStaffById(Long parentId){
        return parentRepository.getById(parentId);
    }

    public Parent updateParent(ParentDto newParentDto){
        Optional<Parent> optionalUser = parentRepository.getParentByUsername(newParentDto.getUsername());
        Parent oldParent = optionalUser.orElseThrow(() ->
                new UsernameNotFoundException("No user found with username : " + newParentDto.getUsername()));

        oldParent.setFirstName(newParentDto.getFirstName());
        oldParent.setMiddleName(newParentDto.getMiddleName());
        oldParent.setLastName(newParentDto.getLastName());

        oldParent.setAddressId(newParentDto.getAddressId());
        oldParent.setStudent(newParentDto.getStudent());

        return parentRepository.save(oldParent);
    }

    public Parent getParentByUsername(String username){
        Optional<Parent> optionalUser = this.parentRepository.getParentByUsername(username);
        return optionalUser.orElseThrow(() -> new UsernameNotFoundException("No Parent found with username :" + username));
    }
}
