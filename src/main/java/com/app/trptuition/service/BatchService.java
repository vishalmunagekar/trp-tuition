package com.app.trptuition.service;

import com.app.trptuition.entity.sql.Batch;
import com.app.trptuition.model.BatchDto;
import com.app.trptuition.repository.sql.BatchRepository;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;
import java.util.Optional;

public class BatchService {

    private BatchRepository batchRepository;

    public Long saveNewBatch(BatchDto batchDto) {
        Batch batch = Batch.builder()
                .batchName(batchDto.getBatchName())
                .isBatchActive(batchDto.isBatchActive())
                .standards(batchDto.getStandards())
                .build();

        return batchRepository.save(batch).getBatchId();
    }

    public List<Batch> getAllBatchs(){
        return batchRepository.findAll();
    }

    public void deleteBatchById(Long batchId){
        batchRepository.deleteById(batchId);
    }

    public Batch getBatchById(Long batchId){
        return batchRepository.getById(batchId);
    }

    public Batch updateBatch(BatchDto newBatchDto){
        Optional<Batch> optionalUser = batchRepository.getBatchByBatchName(newBatchDto.getBatchName());
        Batch oldBatch = optionalUser.orElseThrow(() ->
                new UsernameNotFoundException("No Batch found with newBatchDto : " + newBatchDto.getBatchName()));

        oldBatch.setBatchName(newBatchDto.getBatchName());
        oldBatch.setStandards(newBatchDto.getStandards());
        oldBatch.setBatchActive(newBatchDto.isBatchActive());

        return batchRepository.save(oldBatch);
    }

    public Batch getParentByUsername(String batchName){
        Optional<Batch> optionalUser = this.batchRepository.getBatchByBatchName(batchName);
        return optionalUser.orElseThrow(() -> new UsernameNotFoundException("No Batch found with batchName :" + batchName));
    }
}
