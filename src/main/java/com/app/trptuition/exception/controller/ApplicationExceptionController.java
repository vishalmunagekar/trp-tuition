package com.app.trptuition.exception.controller;


import com.app.trptuition.exception.RecordNotFoundException;
import com.app.trptuition.exception.model.ErrorResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


/**
 * This will give specific error response by building {@link ErrorResponse} Object
 *
 * @author vishal munagekar
 */
@RestControllerAdvice
public class ApplicationExceptionController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(RecordNotFoundException.class)
    private final ResponseEntity<ErrorResponse> handleRecordNotFoundException(RecordNotFoundException exception){
        List<String> errorDetails = new ArrayList<>();
        errorDetails.add(exception.getLocalizedMessage());
        ErrorResponse errorResponse = new ErrorResponse(exception.getMessage(), errorDetails, Instant.now());
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }
}
