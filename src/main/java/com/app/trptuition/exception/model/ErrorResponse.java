package com.app.trptuition.exception.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.List;


/**
 * In application any where any error or exception happen build this
 * object and return via response
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorResponse {
    private String message;
    private String suggestion;
    private List<String> ErrorDetails;
    private Instant timestamp;

    /**
     * Constructs a {@code ErrorResponse} with the specified message and timestamp
     * cause.
     * @param message the detail message.
     * @param timestamp root cause
     */
    public ErrorResponse(String message, Instant timestamp) {
        this.message = message;
        this.timestamp = timestamp;
    }

    /**
     * Constructs a {@code ErrorResponse} with the specified message, error details and timestamp
     * cause.
     * @param message the detail message.
     * @param errorDetails root cause
     * @param timestamp error timestamp
     */
    public ErrorResponse(String message, List<String> errorDetails, Instant timestamp) {
        this.message = message;
        ErrorDetails = errorDetails;
        this.timestamp = timestamp;
    }
}
