package com.app.trptuition.exception;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Thrown if an Specific record not found in database by its id or
 * by any another field.
 *
 * @author vishal munagekar
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class RecordNotFoundException extends RuntimeException {
    /**
     * Constructs a <code>RecordNotFoundException</code> with the specified message.
     * @param msg the detail message.
     */
    public RecordNotFoundException(String msg) {
        super(msg);
    }

    /**
     * Constructs a {@code RecordNotFoundException} with the specified message and root
     * cause.
     * @param msg the detail message.
     * @param cause root cause
     */
    public RecordNotFoundException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
