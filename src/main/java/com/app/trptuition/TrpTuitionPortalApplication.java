package com.app.trptuition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrpTuitionPortalApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrpTuitionPortalApplication.class, args);
	}

}
