package com.app.trptuition.entity.sql;

import com.app.trptuition.entity.nosql.Address;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;


/**
 * This is Entity class for database table name : parent
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "parent")
public class Parent extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long parentId;

    @NotEmpty(message = "First name must not be empty.")
    @Column(length = 50)
    private String firstName;

    @Column(length = 50)
    private String middleName;

    @NotEmpty(message = "Last name must not be empty.")
    @Column(length = 50)
    private String lastName;

    @Column(length = 50)
    private String addressId; // we will add this from mongo

    @OneToOne
    @JoinColumn(name="studentId")
    private Student student; //added this field as OneToOne mapping to Student Table
}
