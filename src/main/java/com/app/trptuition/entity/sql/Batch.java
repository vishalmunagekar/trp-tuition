package com.app.trptuition.entity.sql;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

/**
 * This is Entity class for database table name : batch
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "batch")
public class Batch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long BatchId;

    @OneToMany(mappedBy = "batch", cascade = CascadeType.ALL, orphanRemoval=true,  fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Standard> standards = new ArrayList<>();

    @NotEmpty(message = "Batch name must not be empty.")
    @Column(length = 20)
    private String batchName;

    private boolean isBatchActive;
}
