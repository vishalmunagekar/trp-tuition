package com.app.trptuition.entity.sql;


import com.app.trptuition.entity.nosql.Question;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.NotEmpty;


/**
 * This is Entity class for database table name : topic
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "topic")
public class Topic {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long topicId;

    @Column(length = 50)
    private String questionsId; //we will add here Id of list of que.s

    @NotEmpty(message = "Topic name must not be empty.")
    @Column(length = 150)
    private String topicName;

    @ManyToOne
    @JoinColumn(name = "subjectId")
    private Subject subject;
}
