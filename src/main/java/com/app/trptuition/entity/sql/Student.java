package com.app.trptuition.entity.sql;


import com.app.trptuition.entity.nosql.Address;
import com.app.trptuition.entity.nosql.School;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;
import java.util.List;

/**
 * This is Entity class for database table name : student
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "student")
public class Student extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long studentId;

    @NotEmpty(message = "First name must not be empty.")
    @Column(length = 50)
    private String firstName;

    @NotEmpty(message = "Middle name must not be empty.")
    @Column(length = 50)
    private String middleName;

    @NotEmpty(message = "Last name must not be empty.")
    @Column(length = 50)
    private String lastName;

    @NotEmpty(message = "Admission date must not be empty.")
    @FutureOrPresent(message = "Admission date must be in the present or in the future.")
    private Instant admissionDate;

    @ManyToOne
    @JoinColumn(name="standardId")
    private Standard standard;

    @Column(length = 50)
    private String addressId; // we will add this from mongo

    @OneToOne
    @JoinColumn(name="parentId")
    private Parent parentsDetails;

    @Column(length = 50)
    private String schoolDetailsId; // we will add this from mongo
}
