package com.app.trptuition.entity.sql;


import com.app.trptuition.entity.enums.Gender;
import com.app.trptuition.entity.enums.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import java.time.Instant;
import java.util.Date;


/**
 * This is a base class of {@link Student}, {@link Staff}, {@link Parent}
 * This is not annotated by {@link Entity} instead of it is annotated by {@link MappedSuperclass}
 * This will not create Database entity.
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public abstract class User {

    @NotEmpty(message = "Username must not be empty.")
    @Column(length = 60)
    private String username;

    @NotEmpty(message = "Password must not be empty.")
    private String password;

    @NotEmpty(message = "Gender must not be empty.")
    @Enumerated(EnumType.STRING)
    @Column(length = 6)
    private Gender gender;

    @NotEmpty(message = "Email must not be empty.")
    @Email(message = "Email should be a valid Email.")
    @Column(length = 80)
    private String email;

    @NotEmpty(message = "Mobile Number must not be empty.")
    @Column(length = 10)
    private String mobile;

    @NotEmpty(message = "Birth date must not be empty.")
    @Past(message = "Date of birth cannot be a future date.")
    private Date bday;

    private Instant lastLogin;

    private boolean isActivated;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Role role;
}
