package com.app.trptuition.entity.sql;


import com.app.trptuition.entity.enums.PaperSet;
import com.app.trptuition.entity.nosql.Question;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;


/**
 * This is Entity class for database table name : exam
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "exam")
public class Exam {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long examId;

    @OneToOne
    @JoinColumn(name="standardId")
    private Standard standard;

    @OneToOne
    @JoinColumn(name="subjectId")
    private Subject subject;

    @OneToOne
    @JoinColumn(name="topicId")
    private Topic topic;

    @NotEmpty(message = "Total number of questions must not be empty.")
    private Integer totalQuestions;

    @NotEmpty(message = "Total marks must not be empty.")
    private Float totalMarks;

    @NotEmpty(message = "Exam duration must not be empty.")
    private Instant duration;

    @NotEmpty(message = "Exam date must not be empty.")
    @FutureOrPresent(message = "Exam date must be in the present or in the future.")
    private Instant examDate;

    @NotEmpty(message = "Exam start time must not be empty.")
    @FutureOrPresent(message = "Exam start time must be in the present or in the future.")
    private Instant startTime;

    @OneToOne
    @JoinColumn(name="staffId")
    private Staff paperSetter;

    @Enumerated(EnumType.STRING)
    @Column(length = 5)
    private PaperSet paperSet;

    @Column(length = 50)
    private String questionsId; //we'll add id of all Que.s from mongo db
}
