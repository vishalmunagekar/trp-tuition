package com.app.trptuition.entity.sql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;


/**
 * This is Entity class for database table name : subject
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "subject")
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long subjectId;

    @ManyToMany
    @JoinTable(name = "subject_staff", joinColumns = @JoinColumn(name = "subjectsId"), inverseJoinColumns = @JoinColumn(name = "staffId"))
    private List<Staff> staff = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name="standardId")
    private Standard standard;

    @NotEmpty(message = "Topic name must not be empty.")
    @Column(length = 60)
    private String nameOfSubject;

    @OneToMany(mappedBy = "subject", cascade = CascadeType.ALL)
    private List<Topic> topics = new ArrayList<>();
}
