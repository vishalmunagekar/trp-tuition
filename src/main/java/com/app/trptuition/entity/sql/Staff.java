package com.app.trptuition.entity.sql;

import com.app.trptuition.entity.nosql.Address;
import com.app.trptuition.entity.nosql.Qualification;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * This is Entity class for database table name : staff
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "staff")
public class Staff extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long staffId;

    @NotEmpty(message = "First name must not be empty.")
    @Column(length = 50)
    private String firstName;

    @Column(length = 50)
    private String middleName;

    @NotEmpty(message = "Last name must not be empty.")
    @Column(length = 50)
    private String lastName;

    @NotEmpty(message = "Joining date must not be empty.")
    @FutureOrPresent(message = "Joining date must be in the present or in the future.")
    private Instant joiningDate;

    @OneToMany(mappedBy = "staff", cascade = CascadeType.ALL, orphanRemoval=true,  fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Standard> standards = new ArrayList<>();

    @Column(length = 50)
    private String addressId; //we will add here id of all Address

    @ManyToMany(mappedBy = "staff")
    private List<Subject> subjects = new ArrayList<>();

    @Column(length = 50)
    private String qualificationsId; //we will add here id of all Qualifications
}
