package com.app.trptuition.entity.sql;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;


/**
 * This is Entity class for database table name : standard
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity(name = "standard")
public class Standard {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long standardId;

    @OneToMany(mappedBy = "standard", cascade = CascadeType.ALL, orphanRemoval=true,  fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Student> students = new ArrayList<>();

    @OneToMany(mappedBy = "standard", cascade = CascadeType.ALL, orphanRemoval=true,  fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Subject> subjects = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name="staffId")
    private Staff staff;

    @ManyToOne
    @JoinColumn(name="batchId")
    private Batch batch;

    @NotEmpty(message = "Standard name must not be empty.")
    @Column(length = 20)
    private String standardName;
}
