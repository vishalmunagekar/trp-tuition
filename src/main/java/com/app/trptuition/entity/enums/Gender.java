package com.app.trptuition.entity.enums;

/**
 * This will represent all {@link com.app.trptuition.entity.sql.User}'s Gender types
 *
 * @author vishal munagekar
 */
public enum Gender {
    FEMALE, MALE, OTHER;
}
