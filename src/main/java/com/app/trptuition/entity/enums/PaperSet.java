package com.app.trptuition.entity.enums;


/**
 * This will represent all paper sets
 *
 * @author vishal munagekar
 */
public enum PaperSet {
    A, B, C, D;
}
