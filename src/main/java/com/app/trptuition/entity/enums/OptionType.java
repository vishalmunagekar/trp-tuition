package com.app.trptuition.entity.enums;


/**
 * This will represent all {@link com.app.trptuition.entity.nosql.Option} Types
 *
 * @author vishal munagekar
 */
public enum OptionType {
    A, B, C, D, E;
}
