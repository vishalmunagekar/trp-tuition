package com.app.trptuition.entity.enums;

public enum UserType {
    STUDENT, STAFF, PARENT
}
