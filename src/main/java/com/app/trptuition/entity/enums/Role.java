package com.app.trptuition.entity.enums;

/**
 * This will represent all roles of {@link com.app.trptuition.entity.sql.User}, {@link com.app.trptuition.entity.sql.Student},
 * {@link com.app.trptuition.entity.sql.Staff}, {@link com.app.trptuition.entity.sql.Parent}
 *
 * @author vishal munagekar
 */
public enum Role {
    ADMIN, STUDENT, STAFF, PARENT;
}
