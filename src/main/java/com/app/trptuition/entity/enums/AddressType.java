package com.app.trptuition.entity.enums;


/**
 * This will represent all {@link com.app.trptuition.entity.nosql.Address} types
 *
 * @author vishal munagekar
 */
public enum AddressType {
    TEMPORARY_ADDRESS, PERMANENT_ADDRESS;
}
