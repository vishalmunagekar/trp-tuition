package com.app.trptuition.entity.nosql;


import com.app.trptuition.entity.enums.OptionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


/**
 * This is NoSQL Entity class for database collection name : Question
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "Question")
public class Question {
    @Id
    private String questionId;
    private String question;
    private List<Option> options;
    private OptionType correctAnswer;
}
