package com.app.trptuition.entity.nosql;

import com.app.trptuition.entity.enums.OptionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * This is NoSQL Entity class for database collection name : Option
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "Option")
public class Option {
    @Id
    private String optionId;
    private String optionVal;
    private OptionType optionType;
}
