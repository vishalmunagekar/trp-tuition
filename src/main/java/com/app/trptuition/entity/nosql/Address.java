package com.app.trptuition.entity.nosql;


import com.app.trptuition.entity.enums.AddressType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


/**
 * This is NoSQL Entity class for database collection name : Address
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "Address")
public class Address {
    @Id
    private String addressId;
    private String areaStreet;
    private String landmark;
    private String city;
    private Integer pincode;
    private String state;
    private AddressType addressType;
}
