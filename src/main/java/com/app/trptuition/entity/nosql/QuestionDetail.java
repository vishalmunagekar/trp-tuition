package com.app.trptuition.entity.nosql;


import com.app.trptuition.entity.sql.Standard;
import com.app.trptuition.entity.sql.Subject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * This is NoSQL Entity class for database collection name : QuestionDetail
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "QuestionDetail")
public class QuestionDetail {
    @Id
    private String questionDetailId;
    private Standard standard;
    private Subject subject;
}
