package com.app.trptuition.entity.nosql;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

/**
 * This is NoSQL Entity class for database collection name : Qualification
 *
 * @author vishal munagekar
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Document(collection = "Qualification")
public class Qualification {
    @Id
    private String qualificationId;
    private String university;
    private Instant passingYear;
    private Float experience;
}
