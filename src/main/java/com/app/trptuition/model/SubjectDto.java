package com.app.trptuition.model;

import com.app.trptuition.entity.sql.Staff;
import com.app.trptuition.entity.sql.Standard;
import com.app.trptuition.entity.sql.Topic;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SubjectDto {
    private Long subjectId;
    private String nameOfSubject;
    private List<Staff> staff = new ArrayList<>();
    private Standard standard;
    private List<Topic> topics = new ArrayList<>();
}
