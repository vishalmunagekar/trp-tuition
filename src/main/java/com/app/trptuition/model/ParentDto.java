package com.app.trptuition.model;

import com.app.trptuition.entity.sql.Student;
import com.app.trptuition.entity.sql.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ParentDto extends User {
    private Long parentId;
    private String firstName;
    private String middleName;
    private String lastName;
    private String addressId; // we will add this from mongo
    private Student student;
}
