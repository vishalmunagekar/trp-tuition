package com.app.trptuition.model;

import com.app.trptuition.entity.sql.Batch;
import com.app.trptuition.entity.sql.Staff;
import com.app.trptuition.entity.sql.Student;
import com.app.trptuition.entity.sql.Subject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StandardDto {
    private Long standardId;
    private List<Student> students = new ArrayList<>();
    private List<Subject> subjects = new ArrayList<>();
    private Staff staff;
    private Batch batch;
    private String standardName;
}
