package com.app.trptuition.model;

import com.app.trptuition.entity.sql.Parent;
import com.app.trptuition.entity.sql.Standard;
import com.app.trptuition.entity.sql.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StudentDto extends User {
    private Long studentId;
    private String firstName;
    private String middleName;
    private String lastName;
    private Instant admissionDate;
    private Standard standard;
    private String addressId;
    private Parent parentsDetails;
    private String schoolDetailsId;
}
