package com.app.trptuition.model;

import com.app.trptuition.entity.sql.Subject;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TpicDto {
    private Long topicId;
    private String questionsId;
    private String topicName;
    private Subject subject;
}
