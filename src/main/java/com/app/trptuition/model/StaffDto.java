package com.app.trptuition.model;

import com.app.trptuition.entity.sql.Standard;
import com.app.trptuition.entity.sql.Subject;
import com.app.trptuition.entity.sql.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class StaffDto extends User {
    private Long staffId;
    private String firstName;
    private String middleName;
    private String lastName;
    private Instant joiningDate;
    private List<Standard> standards = new ArrayList<>();
    private String addressId;
    private List<Subject> subjects = new ArrayList<>();
    private String qualificationsId;
}
