package com.app.trptuition.model;

import com.app.trptuition.entity.enums.PaperSet;
import com.app.trptuition.entity.sql.Staff;
import com.app.trptuition.entity.sql.Standard;
import com.app.trptuition.entity.sql.Subject;
import com.app.trptuition.entity.sql.Topic;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ExamDto {
    private Long examId;
    private Standard standard;
    private Subject subject;
    private Topic topic;
    private Integer totalQuestions;
    private Float totalMarks;
    private Instant duration;
    private Instant examDate;
    private Instant startTime;
    private Staff paperSetter;
    private PaperSet paperSet;
    private String questionsId;
}
