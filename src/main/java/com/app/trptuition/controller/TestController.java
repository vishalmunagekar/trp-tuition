package com.app.trptuition.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.cache.SpringCacheBasedUserCache;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestController.class);

    @RequestMapping(value = "/test/{username}",method = RequestMethod.GET)
    public String test(@PathVariable String username){
//        System.out.println("Current Directory = " + System.getProperty("user.dir"));
//        logger.trace("A TRACE Message");
//        logger.debug("A DEBUG Message");
//        logger.info("An INFO Message");
//        logger.warn("A WARN Message");
//        logger.error("An ERROR Message");
        LOGGER.debug("url : /test/{}", username);
        return "Welcome " + username + " to Test Controller ";
    }

//    @RequestMapping(value = "/auth", method = RequestMethod.POST)
//    public ResponseEntity<?> createAuthToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception{
//
//    }
}
