## SQL Query
```sql
INSERT INTO `TRP-TUITION`.`staff` 
(`staff_id`, `bday`, `email`, `gender`, `is_activated`, `last_login`, `mobile`, `password`, `role`, `username`, `address_id`, `first_name`, `joining_date`, `last_name`, `middle_name`, `qualifications_id`)
VALUES ('1', current_date(), 'vishal@gmail.com', 'MALE', 1, current_date(), '7843052772', 'qwertyui', 'ADMIN', 'vishalm', '', 'vishal', current_timestamp(), 'munagekar', 'vitthal', '');
```

## To unistall MySQL
```shell script
sudo apt-get remove --purge mysql*
sudo apt-get purge mysql*
sudo apt-get autoremove
sudo apt-get autoclean
sudo apt-get remove dbconfig-mysql
sudo apt-get dist-upgrade
```

## To istall MySQL
```shell script
sudo apt-get install mysql-server
mysql -u root -p manager
```
### db credentials
* **user**      - root
* **password**  - manager


## Mondgo DB system controlls
```shell script
sudo systemctl status mongod

sudo systemctl stop mongod

sudo systemctl start mongod

sudo systemctl restart mongod

sudo systemctl disable mongod

sudo systemctl enable mongod

mongo --eval 'db.runCommand({ connectionStatus: 1 })'
```

## chmod cmd dtails
* r (read) = 4
* w (write) = 2
* x (execute) = 1
* no permissions = 0

* Owner: rwx=4+2+1=7
* Group: r-x=4+0+1=5
* Others: r-x=4+0+0=4